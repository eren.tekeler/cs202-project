package edu.ozyegin.cs.entity;

import com.google.common.base.Objects;

public class SchedulesAndBackUpTrains {
    private int ScheduleId;
    private int BackUpTrainId;

    public SchedulesAndBackUpTrains() {
    }

    public int getScheduleId() {
        return ScheduleId;
    }

    public void setScheduleId(int scheduleId) {
        ScheduleId = scheduleId;
    }

    public int getBackUpTrainId() {
        return BackUpTrainId;
    }

    public void setBackUpTrainId(int backUpTrainId) {
        BackUpTrainId = backUpTrainId;
    }

}