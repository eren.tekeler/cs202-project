package edu.ozyegin.cs.entity;

import com.google.common.base.Objects;

public class Stops {
    private int StopId;
    private String StopName;

    public Stops() {
    }

    public int getStopId() {
        return StopId;
    }

    public void setStopId(int stopId) {
        this.StopId = stopId;
    }

    public String getStopName() {
        return StopName;
    }

    public void setStopName(String stopName) {
        this.StopName = stopName;
    }

}