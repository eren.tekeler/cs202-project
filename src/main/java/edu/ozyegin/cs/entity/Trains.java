package edu.ozyegin.cs.entity;

import com.google.common.base.Objects;

public class Trains {
    private int TrainId;
    private String TrainName;

    public Trains() {
    }


    public int getTrainId() {
        return TrainId;
    }

    public void setTrainId(int trainId) {
        this.TrainId = trainId;
    }

    public String getTrainName() {
        return TrainName;
    }

    public void setTrainName(String trainName) {
        this.TrainName = trainName;
    }

}