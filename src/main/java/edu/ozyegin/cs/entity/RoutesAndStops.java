package edu.ozyegin.cs.entity;

import com.google.common.base.Objects;

public class RoutesAndStops {
    private int RouteId;
    private int StopId;

    public RoutesAndStops() {
    }

    public int getRouteId() {
        return RouteId;
    }

    public void setRouteId(int routeId) {
        RouteId = routeId;
    }

    public int getStopId() {
        return StopId;
    }

    public void setStopId(int stopId) {
        this.StopId = stopId;
    }


}