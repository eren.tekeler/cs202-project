package edu.ozyegin.cs.entity;

import com.google.common.base.Objects;

public class Routes {
    private int RouteId;
    private String RouteName;

    public Routes() {
    }

    public int getRouteId() {
        return RouteId;
    }

    public void setRouteId(int routeId) {
        this.RouteId = routeId;
    }

    public String getRouteName() {
        return RouteName;
    }

    public void setRouteName(String routeName) {
        this.RouteName = routeName;
    }

    public int getRouteIdByName(String routename) {
        if (this.RouteName.equals(routename)) {
            return this.RouteId;
        }
        return 0;
    }

}