package edu.ozyegin.cs.controller;


import edu.ozyegin.cs.entity.Schedules;
import edu.ozyegin.cs.entity.Stops;
import edu.ozyegin.cs.entity.Trains;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.*;

@RestController
@RequestMapping
@CrossOrigin
public class TrainsController {
    @Autowired
    private PlatformTransactionManager transactionManager;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    final String createTrainPs = "INSERT INTO trains(TrainName) VALUES (?)";
    final String modifyTrainPs = "UPDATE trains SET TrainName=? WHERE TrainId=?";
    final String deleteTrainPs = "DELETE FROM trains Where TrainId=?";
    final String getAllTrainsPs = "SELECT * FROM trains";
    final String getBackupTrainsWithAtLeastKSchedulesQuery = "Select t1.BackUpTrainID, t.TrainName  From (SELECT BackUpTrainID, count(Distinct ScheduleId) as ScheduleIdCount FROM schedulesandbackuptrains " +
            "GROUP BY BackUpTrainID having ScheduleIdCount>= ?) as t1, trains as t " +
            "Where t1.BackUpTrainId = t.TrainId";
    final String getMaxUsedTrainQuery = "Select t2.maxId From (Select t1.TrainId as maxId, max(t1.TrainIdCount) From (SELECT TrainId, count(TrainId) as TrainIdCount FROM schedules GROUP BY TrainId) as t1) as t2";
    final String getBackupTrainPassesGivenStopQuery = "Select Distinct sab.BackUpTrainID From schedules as s, routesandstops as ras,routes as r, trains as t, schedulesandbackuptrains sab " +
            "Where s.RouteId = ras.RouteId and sab.ScheduleId= s.ScheduleId and ras.StopId = ?";
    final int batchSize = 10;


    @RequestMapping(value = "/train/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity createTrain(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Trains> trains = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Trains train = new Trains();
            train.setTrainName((String) entity.get("TrainName"));

            trains.add(train);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(createTrainPs, trains, batchSize,
                    (ps, train) -> {
                        ps.setString(1, train.getTrainName());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed creating Train");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/train/modify/rename", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity modifyTrain(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Trains> trains = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Trains train = new Trains();
            train.setTrainId((int) entity.get("TrainId"));
            train.setTrainName((String) entity.get("TrainName"));

            trains.add(train);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(modifyTrainPs, trains, batchSize,
                    (ps, train) -> {
                        ps.setString(1, train.getTrainName());
                        ps.setInt(2, train.getTrainId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed renaming Train");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    @RequestMapping(value = "/train/modify/delete", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity deleteTrain(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Trains> trains = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Trains train = new Trains();
            train.setTrainId((int) entity.get("TrainId"));

            trains.add(train);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(deleteTrainPs, trains, batchSize,
                    (ps, train) -> {
                        ps.setInt(1, train.getTrainId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed deleting Train");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/train/get_all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllTrains() {
        try {
            List<Trains> data = Objects.requireNonNull(jdbcTemplate).query(getAllTrainsPs, new BeanPropertyRowMapper<>(Trains.class));

            Map<String, Object> response = new HashMap<>();
            response.put("trains", data);
            response.put("status", true);

            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
        }
    }

    @RequestMapping(value = "/statistics/train/backups", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getBackupTrainsWithAtLeastKSchedules(@RequestBody Map<String, Object>[] payload) {
        List<Integer> Ks = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            int K = (int)entity.get("K");
            Ks.add(K);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Integer k : Ks) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getBackupTrainsWithAtLeastKSchedulesQuery,
                        new Object[]{k});
                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("Ids", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Trains");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistics/train/max_main", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getMaxUsedTrain() {
        try {
            List<Trains> data = Objects.requireNonNull(jdbcTemplate).query(getMaxUsedTrainQuery, new BeanPropertyRowMapper<>(Trains.class));

            Map<String, Object> response = new HashMap<>();
            response.put("Id", data);
            response.put("status", true);

            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
        }
    }

    @RequestMapping(value = "/statistics/train/stop_backup", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getBackupTrainPassesGivenStop(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Stops> stops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Stops stop = new Stops();
            stop.setStopId((int) entity.get("StopId"));

            stops.add(stop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Stops s : stops) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getBackupTrainPassesGivenStopQuery,
                        new Object[]{s.getStopId()});
                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("Ids", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Trains");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
