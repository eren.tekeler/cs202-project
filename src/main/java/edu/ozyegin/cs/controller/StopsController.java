package edu.ozyegin.cs.controller;

import edu.ozyegin.cs.entity.Sample;
import edu.ozyegin.cs.entity.Stops;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.*;

@RestController
@RequestMapping
@CrossOrigin
public class StopsController {
    @Autowired
    private PlatformTransactionManager transactionManager;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    final String createStopPs = "INSERT INTO stops(StopName) VALUES (?)";
    final String modifyStopPs = "UPDATE stops SET StopName = ? WHERE StopId = ?";
    final String deleteStopPs = "DELETE FROM stops Where StopId = ?";
    final String getAllStopsPs = "SELECT * FROM stops";

    final int batchSize = 10;


    @RequestMapping(value = "/stop/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity createStop(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Stops> stops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Stops stop = new Stops();
            stop.setStopName((String) entity.get("StopName"));

            stops.add(stop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(createStopPs, stops, batchSize,
                    (ps, stop) -> {
                        ps.setString(1, stop.getStopName());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed creating Stop");
        }

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/stop/modify/rename", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity modifyStop(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Stops> stops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Stops stop = new Stops();
            stop.setStopId((int) entity.get("StopId"));
            stop.setStopName((String) entity.get("StopName"));

            stops.add(stop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(modifyStopPs, stops, batchSize,
                    (ps, stop) -> {
                        ps.setString(1, stop.getStopName());
                        ps.setInt(2, stop.getStopId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed renaming Stop");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    @RequestMapping(value = "/stop/modify/delete", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity deleteStop(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Stops> stops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Stops stop = new Stops();
            stop.setStopId((int) entity.get("StopId"));

            stops.add(stop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(deleteStopPs, stops, batchSize,
                    (ps, stop) -> {
                        ps.setInt(1, stop.getStopId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed deleting Stop");
        }

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/stop/get_all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllStops() {
        try {
            List<Stops> data = Objects.requireNonNull(jdbcTemplate).query(getAllStopsPs, new BeanPropertyRowMapper<>(Stops.class));

            Map<String, Object> response = new HashMap<>();
            response.put("stops", data);
            response.put("status", true);

            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
        }
    }
}
