package edu.ozyegin.cs.controller;


import edu.ozyegin.cs.entity.RoutesAndStops;
import edu.ozyegin.cs.entity.Schedules;
import edu.ozyegin.cs.entity.SchedulesAndBackUpTrains;
import edu.ozyegin.cs.entity.Stops;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.*;

@RestController
@RequestMapping
@CrossOrigin
public class SchedulesController {
    @Autowired
    private PlatformTransactionManager transactionManager;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    final String createSchedulePs = "INSERT INTO schedules(RouteId, TrainId , Time) VALUES (? , ?, ?)";
    final String modifyScheduleTimePs = "UPDATE schedules SET Time=? WHERE ScheduleId=?";
    final String modifyScheduleRoutePs = "UPDATE schedules SET RouteId=? WHERE ScheduleId=?";
    final String modifyScheduleTrainPs = "UPDATE schedules SET TrainId=? WHERE ScheduleId=?";
    final String addBackupTrainSchedulePs = "INSERT INTO schedulesandbackuptrains(ScheduleId, BackUpTrainId) VALUES (?, ?)";
    final String removeBackupTrainSchedulePs = "DELETE FROM schedulesandbackuptrains Where ScheduleId=? and BackUpTrainID=?";
    final String deleteSchedulePs = "DELETE FROM schedules Where ScheduleId=?";
    final String getAllSchedulePs = "SELECT * FROM schedules";
    final String getSchedulesOnGivenTimeQuery = "Select distinct RouteId, TrainId , Time From schedules Where Time = ? ";
    final String getSchedulesUsingGivenRouteQuery = "Select distinct ScheduleId, RouteId, TrainId, Time From schedules Where RouteId=?";
    final String getSchedulesTimeandStopQuery = "Select s. ScheduleId, s.RouteId, s.TrainId, s.Time From schedules as s, routesandstops as ras\n" +
            "Where s.Time = ? and s.RouteId = ras.RouteId and ras.StopId = ?";
    final String getSchedulesWithAtLeastKBackupQuery = "Select t1. ScheduleId, t1.RouteId, t1.TrainId, t1.Time " +
            "From (Select s.ScheduleId, s.RouteId, s.TrainId, r.RouteName,t.TrainName,s.Time From schedules as s, routes as r, trains as t Where s.Time = Time and s.RouteId = r.RouteId and s.TrainId=t.TrainId) as t1 " +
            "INNER JOIN (SELECT ScheduleId, count(BackUpTrainID) as BackUpTrainCount FROM schedulesandbackuptrains GROUP BY ScheduleId having BackUpTrainCount>=?) as t2 " +
            "ON t1.ScheduleId = t2.ScheduleId";
    final int batchSize = 10;


    @RequestMapping(value = "/schedule/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity createSchedule(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setRouteId((int) entity.get("RouteId"));
            schedule.setTrainId((int) entity.get("TrainId"));
            schedule.setTime((String) entity.get("Time"));

            schedules.add(schedule);
        }
        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(createSchedulePs, schedules, batchSize,
                    (ps, schedule) -> {
                        ps.setInt(1, schedule.getRouteId());
                        ps.setInt(2, schedule.getTrainId());
                        ps.setString(3, schedule.getTime());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed creating Schedule");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/schedule/modify/change_time", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity modifyScheduleTime(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setScheduleId((int) entity.get("ScheduleId"));
            schedule.setTime((String) entity.get("Time"));

            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(modifyScheduleTimePs, schedules, batchSize,
                    (ps, schedule) -> {
                        ps.setString(1, schedule.getTime());
                        ps.setInt(2, schedule.getScheduleId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed modifying Schedule");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    @RequestMapping(value = "/schedule/modify/change_route", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity modifyScheduleRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setScheduleId((int) entity.get("ScheduleId"));
            schedule.setRouteId((int) entity.get("RouteId"));

            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(modifyScheduleRoutePs, schedules, batchSize,
                    (ps, schedule) -> {
                        ps.setInt(1, schedule.getRouteId());
                        ps.setInt(2, schedule.getScheduleId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed modifying Schedule");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/schedule/modify/change_train", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity modifyScheduleTrain(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setScheduleId((int) entity.get("ScheduleId"));
            schedule.setTrainId((int) entity.get("TrainId"));

            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(modifyScheduleTrainPs, schedules, batchSize,
                    (ps, schedule) -> {
                        ps.setInt(1, schedule.getTrainId());
                        ps.setInt(2, schedule.getScheduleId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed modifying Schedule");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/schedule/modify/add_backup_train", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity addBackUpTrainSchedule(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<SchedulesAndBackUpTrains> schedulesandbackuptrains = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            SchedulesAndBackUpTrains scheduleandbackuptrain = new SchedulesAndBackUpTrains();
            scheduleandbackuptrain.setScheduleId((int) entity.get("ScheduleId"));
            scheduleandbackuptrain.setBackUpTrainId((int) entity.get("BackUpTrainId"));

            schedulesandbackuptrains.add(scheduleandbackuptrain);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(addBackupTrainSchedulePs, schedulesandbackuptrains, batchSize,
                    (ps, scheduleandbackuptrain) -> {
                        ps.setInt(1, scheduleandbackuptrain.getScheduleId());
                        ps.setInt(2, scheduleandbackuptrain.getBackUpTrainId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed adding Backup Train");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/schedule/modify/remove_backup_train", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity removeBackUpTrainSchedule(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<SchedulesAndBackUpTrains> schedulesandbackuptrains = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            SchedulesAndBackUpTrains scheduleandbackuptrain = new SchedulesAndBackUpTrains();
            scheduleandbackuptrain.setScheduleId((int) entity.get("ScheduleId"));
            scheduleandbackuptrain.setBackUpTrainId((int) entity.get("BackUpTrainId"));

            schedulesandbackuptrains.add(scheduleandbackuptrain);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(removeBackupTrainSchedulePs, schedulesandbackuptrains, batchSize,
                    (ps, schedulesandbackuptrain) -> {
                        ps.setInt(1, schedulesandbackuptrain.getScheduleId());
                        ps.setInt(2, schedulesandbackuptrain.getBackUpTrainId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed removing Backup Train");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/schedule/delete", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity deleteSchedule(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setScheduleId((int) entity.get("ScheduleId"));

            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(deleteSchedulePs, schedules, batchSize,
                    (ps, schedule) -> {
                        ps.setInt(1, schedule.getScheduleId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed deleting Schedule");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    @RequestMapping(value = "/schedule/get_all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllSchedules() {
        try {
            List<Schedules> data = Objects.requireNonNull(jdbcTemplate).query(getAllSchedulePs, new BeanPropertyRowMapper<>(Schedules.class));

            Map<String, Object> response = new HashMap<>();
            response.put("schedules", data);
            response.put("status", true);

            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
        }
    }

    @RequestMapping(value = "/statistics/schedules/time", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getSchedulesOnGivenTime(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setTime((String) entity.get("Time"));

            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Schedules schedule : schedules) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getSchedulesOnGivenTimeQuery,
                        new Object[]{schedule.getTime()});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("schedules", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Schedules");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistics/schedules/route", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getSchedulesUsingGivenRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setRouteId((int) entity.get("RouteId"));

            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Schedules schedule : schedules) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getSchedulesUsingGivenRouteQuery,
                        new Object[]{schedule.getRouteId()});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("schedules", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Schedules");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistics/schedules/time_stop", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getSchedulesTimeAndStop(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        List<Stops> stops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            Stops stop = new Stops();
            schedule.setTime((String) entity.get("Time"));
            stop.setStopId((int) entity.get("StopId"));

            stops.add(stop);
            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Schedules schedule : schedules) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getSchedulesTimeandStopQuery,
                        new Object[]{schedule.getTime() , stops.get(schedules.indexOf(schedule)).getStopId()});
                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("schedules", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Schedules");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistics/schedules/backup_count", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getSchedulesWithAtLeastKBackup(@RequestBody Map<String, Object>[] payload) {
        List<Integer> Ks = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            int K = (int)entity.get("K");
            Ks.add(K);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Integer k : Ks) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getSchedulesWithAtLeastKBackupQuery,
                        new Object[]{k});
                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("schedules", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Schedules");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

}
