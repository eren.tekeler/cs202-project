package edu.ozyegin.cs.controller;

import edu.ozyegin.cs.entity.Routes;
import edu.ozyegin.cs.entity.RoutesAndStops;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.*;

@RestController
@RequestMapping
@CrossOrigin
public class RoutesController {
    @Autowired
    private PlatformTransactionManager transactionManager;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    final String createRoutePs1 = "INSERT INTO Routes(RouteName) VALUES (?)";
    final String createRoutePs2 = "INSERT INTO RoutesAndStops(RouteId, StopId) VALUES (? , ?)";
    final String modifyRoutePs = "UPDATE routes SET RouteName= ? WHERE RouteId = ?";
    final String addStopRoutePs = "INSERT INTO RoutesAndStops(RouteId, StopId) VALUES (?, ?)";
    final String removeStopRoutePs = "DELETE FROM RoutesAndStops Where RouteId=? and StopId=?";
    final String deleteRoutePs = "DELETE FROM routes Where RouteId=?";
    final String getRoutePs = "SELECT s2.RouteId, s2.RouteName, s2.StopId, s1.StopName FROM stops as s1\n" +
            "INNER JOIN (SELECT ras.StopId, ras.RouteId, r.RouteName FROM RoutesAndStops as ras, routes as r\n" +
            "WHERE ras.RouteId = ? and ras.RouteId = r.RouteId) as s2 ON s1.StopId = s2.StopId";

    final String getAllRoutePs = "SELECT s2.RouteName, s2.StopId, s1.StopName FROM stops as s1\n" +
            "INNER JOIN (SELECT StopId, RouteName FROM RoutesAndStops) as s2\n" +
            "ON s1.StopId = s2.StopId ORDER BY RouteName ASC";

    final int batchSize = 10;


  @RequestMapping(value = "/route/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity createRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Routes> routes = new ArrayList<>();
        List<RoutesAndStops> routesandstops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Routes route = new Routes();
            RoutesAndStops routeandstop = new RoutesAndStops();

            route.setRouteName((String) entity.get("RouteName"));
            int[] stop_ids = (int[]) entity.get("stop_ids");

            for(int i = 0; i<stop_ids.length;i++){
                routeandstop.setStopId(stop_ids[i]);
                routeandstop.setRouteId(route.getRouteIdByName((String) entity.get("RouteName")));
                routesandstops.add(routeandstop);
            }
            routes.add(route);
        }
        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(createRoutePs1, routes, batchSize,
                    (ps, route) -> {
                        ps.setString(1, route.getRouteName());
                    });
            Objects.requireNonNull(jdbcTemplate).batchUpdate(createRoutePs2, routesandstops, batchSize,
                    (ps, routeandstop) -> {
                        ps.setInt(1, routeandstop.getRouteId());
                        ps.setInt(2, routeandstop.getStopId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed creating Route");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/route/modify/rename", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity modifyRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Routes> routes = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Routes route = new Routes();
            route.setRouteId((int) entity.get("RouteId"));
            route.setRouteName((String) entity.get("RouteName"));

            routes.add(route);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(modifyRoutePs, routes, batchSize,
                    (ps, route) -> {
                        ps.setString(1, route.getRouteName());
                        ps.setInt(2, route.getRouteId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed renaming Route");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    @RequestMapping(value = "/route/modify/add_stop", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity addStopRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<RoutesAndStops> routesandstops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            RoutesAndStops routeandstop = new RoutesAndStops();
            routeandstop.setRouteId(((int) entity.get("RouteId")));
            routeandstop.setStopId((int) entity.get("StopId"));

            routesandstops.add(routeandstop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(addStopRoutePs, routesandstops, batchSize,
                    (ps, routeandstop) -> {
                        ps.setInt(1, routeandstop.getRouteId());
                        ps.setInt(2, routeandstop.getStopId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed inserting Stops");
        }

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/route/modify/remove_stop", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity removeStopRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<RoutesAndStops> routesandstops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            RoutesAndStops routeandstop = new RoutesAndStops();
            routeandstop.setRouteId(((int) entity.get("RouteId")));
            routeandstop.setStopId((int) entity.get("StopId"));

            routesandstops.add(routeandstop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(removeStopRoutePs, routesandstops, batchSize,
                    (ps, routeandstop) -> {
                        ps.setInt(1, routeandstop.getRouteId());
                        ps.setInt(2, routeandstop.getStopId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed removing Stops");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/route/delete", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity deleteRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Routes> routes = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Routes route = new Routes();
            route.setRouteId(((int) entity.get("RouteId")));

            routes.add(route);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            Objects.requireNonNull(jdbcTemplate).batchUpdate(deleteRoutePs, routes, batchSize,
                    (ps, route) -> {
                        ps.setInt(1, route.getRouteId());
                    });

            // commit changes to database
            transactionManager.commit(txStatus);

            response.put("success", true);  // prepare data to respond with
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("success", false);
            response.put("message", "Failed deleting Route");
        }

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/route/get", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getRoute(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<RoutesAndStops> routesandstops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            RoutesAndStops routeandstop = new RoutesAndStops();
            routeandstop.setRouteId(((int) entity.get("RouteId")));

            routesandstops.add(routeandstop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for(RoutesAndStops ras: routesandstops) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getRoutePs,
                        new Object[]{ras.getRouteId()});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("route", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Route");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/route/get_all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllRoute() {
        try {
            List<Routes> data = Objects.requireNonNull(jdbcTemplate).query(getAllRoutePs, new BeanPropertyRowMapper<>(Routes.class));

            Map<String, Object> response = new HashMap<>();
            response.put("routes", data);
            response.put("status", true);

            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
        }
    }
}
