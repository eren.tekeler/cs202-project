package edu.ozyegin.cs.controller;

import edu.ozyegin.cs.entity.RoutesAndStops;
import edu.ozyegin.cs.entity.Sample;
import edu.ozyegin.cs.entity.Schedules;
import edu.ozyegin.cs.entity.Stops;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.*;

@RestController
@RequestMapping
@CrossOrigin
public class RoutesAndStopsController {
    @Autowired
    private PlatformTransactionManager transactionManager;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    final String getRoutesPassesStopPs = "Select r.RouteId FROM Routes as r INNER JOIN (Select RouteId From routesandstops Where StopId = ? as n ON r.RouteId= n.RouteId";

    final String getRouteSizeAtLeastkStopQuery = "SELECT RouteId FROM routesandstops as ras GROUP BY RouteId having Count(ras.StopId) >= ?";

    final String getStopsIncludedAtLeastkRoutesQuery = "SELECT StopId FROM routesandstops GROUP BY StopId having count(Distinct RouteId) >= ?";

    final String getStopsonGivenTimeQuery = "Select distinct ras.StopId From schedules as s,routesandstops as ras Where s.Time=? and s.RouteId = ras.RouteId";
    final String getSchedulesPassesGivenStopQuery = "Select distinct s.ScheduleId, s.RouteId, s.TrainId ,s.Time From schedules as s, routesandstops as ras Where ras.StopId = ? and ras.RouteId = s.RouteId";

    final int batchSize = 10;


    @RequestMapping(value = "/statistics/route/stop", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getRoutesPassesStop(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<RoutesAndStops> routesandstops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            RoutesAndStops routeandstop = new RoutesAndStops();
            routeandstop.setStopId((int) entity.get("StopId"));

            routesandstops.add(routeandstop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (RoutesAndStops ras : routesandstops) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getRoutesPassesStopPs,
                        new Object[]{ras.getStopId()});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("Ids", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting RouteIds");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    @RequestMapping(value = "/statistics/route/size", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getRouteSizeAtLeastKStop(@RequestBody Map<String, Object>[] payload) {
        List<Integer> Ks = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            int K = (int) entity.get("K");
            Ks.add(K);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Integer k : Ks) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getRouteSizeAtLeastkStopQuery,
                        new Object[]{k});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("Ids", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting RouteIds");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistics/stop/size", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getStopsIncludedAtLeastKRoutes(@RequestBody Map<String, Object>[] payload) {
        List<Integer> Ks = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            int K = (int) entity.get("K");
            Ks.add(K);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Integer k : Ks) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getStopsIncludedAtLeastkRoutesQuery,
                        new Object[]{k});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("Ids", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting StopIds");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistics/stop/time", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getStopsOnGivenTime(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<Schedules> schedules = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            Schedules schedule = new Schedules();
            schedule.setTime((String) entity.get("Time"));

            schedules.add(schedule);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (Schedules schedule : schedules) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getStopsonGivenTimeQuery,
                        new Object[]{schedule.getTime()});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("Ids", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting StopIds");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @RequestMapping(value = "/statistics/schedule/stop", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity getSchedulesPassesGivenStop(@RequestBody Map<String, Object>[] payload) {
        // prepare data for usage
        List<RoutesAndStops> routesandstops = new ArrayList<>();
        for (Map<String, Object> entity : payload) {
            RoutesAndStops routeandstop = new RoutesAndStops();
            routeandstop.setStopId((int) entity.get("StopId"));

            routesandstops.add(routeandstop);
        }

        // init Transaction Manager
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        // create response's structure
        Map<String, Object> response = new HashMap<>();

        try {
            // INSERT INTO Samples using a PREPARED STATEMENT
            for (RoutesAndStops ras : routesandstops) {
                List<Map<String, Object>> data = Objects.requireNonNull(jdbcTemplate).queryForList(getSchedulesPassesGivenStopQuery,
                        new Object[]{ras.getStopId()});

                // commit changes to database
                transactionManager.commit(txStatus);
                // prepare data to respond with
                response.put("schedules", data);
            }
        } catch (Exception exception) {
            // revert changes planned
            transactionManager.rollback(txStatus);

            // prepare data to respond with
            response.put("message", "Failed getting Schedules");
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

}

