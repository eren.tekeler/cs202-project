create table if not exists Sample(
    id serial PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    data text,
    value int default 0
);

create table if not exists Stops
(
    StopId   int AUTO_INCREMENT PRIMARY KEY,
    StopName VARCHAR(64) NOT NULL,
    unique(StopName)
);

create table if not exists Routes
(
    RouteId   int AUTO_INCREMENT PRIMARY KEY,
    RouteName VARCHAR(64) NOT NULL,
    unique(RouteName)
);

create table if not exists RoutesAndStops
(
    RouteId int ,
    StopId  int ,
    unique(RouteId, StopId),
    FOREIGN KEY (StopId) REFERENCES Stops (StopId)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (RouteId) REFERENCES Routes (RouteId)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

create table if not exists Trains
(
    TrainId   int AUTO_INCREMENT PRIMARY KEY,
    TrainName VARCHAR(64) NOT NULL,
    unique(TrainName)
);

create table if not exists Schedules
(
    ScheduleId    int AUTO_INCREMENT PRIMARY KEY,
    RouteId     int NOT NULL,
    TrainId     int NOT NULL,
    Time          time NOT NULL,
    FOREIGN KEY (RouteId) REFERENCES Routes (RouteId)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (TrainId) REFERENCES Trains (TrainId)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

create table if not exists SchedulesAndBackUpTrains
(
    ScheduleId    int ,
    BackUpTrainID int ,
    unique(ScheduleId, BackUpTrainID),
    FOREIGN KEY (ScheduleId) REFERENCES Schedules (ScheduleId)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (BackUpTrainID) REFERENCES Trains (TrainId)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
